keras feature extraction using ResNet50.

Dataset from [rice leaf diseases](https://www.kaggle.com/vbookshelf/rice-leaf-diseases).

Reference code [feature extraction](https://www.pyimagesearch.com/2019/05/27/keras-feature-extraction-on-large-datasets-with-deep-learning/).
